+++
title = "About"
template = "index.html"
+++

---
# 公開鍵
[public.gpg](public.gpg)

# 言語
- プログラミング言語
    - C
    - C++
    - Rust
    - Elm
    - TypeScript
    - など

# アニメと小説
- 伊藤計劃
- 武田綾乃
- その他SFやミステリや百合

# ゲーム
- Counter Strike
- Minecraft

# お絵かき
- ごくまれ

# 3DCG
- Blender

# その他
- 分散型SNS
- 料理
- 
- 労働の廃止

# 連絡先
- GitLab: [@nazuh](https://gitlab.com/nazuh)
- Twitter: [@5epe](https://twitter.com/5epe)